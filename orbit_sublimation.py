# IMPORT

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy.stats as sts
import vtk
from scipy.spatial import ConvexHull
import orbit_function_toolbox as oft
import vtk

# KEY PARAMETERS

#shape_model = ['cg-spc-shap7-v1.8-cheops-006k.obj', 'sphere2.obj', 'Hartley_2_EPOXI.obj', 'Wild_2_Stardust_NAVCAM.obj', 'Halley_Giotto_Vega_Stooke_Model_1.obj', 'Tempel_1_DeepImpact_Stardust.obj']
shape_model = ['Wild_2_Stardust_NAVCAM.obj']
#shape_model = 'sphere2.obj'
#shape_model = ['Hartley_2_EPOXI.obj']
#shape_model = 'Wild_2_Stardust_NAVCAM.obj'

semimajoraxis= 3.442 #<-81P #3.122 #<-9P #3.470 #<-103P #3.4630 #<-67P # 17.83 # 3.4630 # * 1.496e8
ecc= 0.540 #<-81P #0.517 #<-9P #0.695 #<-103P 0.64102 #<-67P # 0.967 #0.64102
tilt= [0., 30., 45., 60., 90.] #, 60.] #* np.pi/180.
off_axis_tilt = [90.] #np.pi*90./180.
flux_percent = 1.

#Activity distribution parameters:

random_activity_threshhold = 0.8
# facet values above this value are active
random_flag = 0
# if == 0: all values == 1; 
# if == 1: randomly distirbuted values between 0. - 1.
# if == 2: values are 0. or 1. , determined by limits below
x_lowlim = -1.5
x_hilim = 1.5
y_lowlim = -1.5
y_hilim = 1.5
z_lowlim = -0.2
z_hilim = 0.2

#Orbit parameters

semiminoraxis=semimajoraxis*(np.sqrt(1-ecc**2))
peri=semimajoraxis*(1.-ecc)
orbint=np.pi/25
orbint_day = np.pi/25
rh_steps = int(2*np.pi/orbint) + 2
day_steps = int(2*np.pi/orbint_day) + 2
print rh_steps, day_steps

# CONSTANTS

emissivity = 0.9
stef_bol = 5.670e-8
R = 8.31446
mol_water = 0.018015 #kg
mol_co2 = 0.04401
Po = 3.56e12
T_top = 6141.667
Lice = 2.6e6
Lice_co2 = 577000.
so=1367.

# PARAMETERS

###Buggy if both Nd and Nr are ten or greater need fix for this...
#Nd=10 #works up to 99
#Nr=9
#Num=Nr*(Nd-1)+2
#semiminoraxis=semimajoraxis*(np.sqrt(1-ecc**2))
#peri=semimajoraxis*(1.-ecc)
#auz=1.
#r=2. #/1.5e8 # in au for 2 km
#non_sph = 0.
#orb=0.
#orbint=np.pi/25
#orbint_day = np.pi/25
#rh_steps = int(2*np.pi/orbint) + 2
#day_steps = int(2*np.pi/orbint_day) + 2
#print rh_steps, day_steps

# DEFINITIONS

### DEFINITIONS FOR FTZ


# READ FLUX, TEMP, Z DATA & PLOT IT
"""
VPT = []
VPW = []
VPC = []
VPW2 = []
for i in range(150, 300):
	vpt = float(i)
	vpw = oft.general_vapour_pressure(vpt, 'H2O')
	vpc = oft.general_vapour_pressure(vpt, 'CO2')
	vpw2 = oft.water_vapor_pressure(vpt)
	VPT.append(vpt)
	VPC.append(vpc)
	VPW.append(vpw)
	VPW2.append(vpw2)
plt.plot(VPT, VPW, label='gen water')
plt.plot(VPT, VPW2, label='spec water')
plt.plot(VPT, VPC, label='gen co2')
plt.legend()
plt.show()
"""

FLUX=[]
TGRY=[]
TLOW=[]
TAVG=[]
ZLOW=[]
ZAVG=[]
ro=open('./CalcFTZ_higherF.txt','r')
for line in ro:
        F, Tg, Ta , Tb , Za, Zb = line.split('   ')
        FLUX.append(float(F))
        TGRY.append(float(Tg))
        TLOW.append(float(Ta))
        TAVG.append(float(Tb))
        ZLOW.append(float(Za))
        ZAVG.append(float(Zb))


plt.figure('FTZ')
plt.plot(FLUX, TLOW)
radiated_flux = [oft.rad_flux(a) for a in TLOW]
sub_flux = [oft.ZT_Lice(oft.water_vapor_pressure(a), a, mol_water, Lice) for a in TLOW]
plt.plot(radiated_flux, TLOW)
plt.plot(sub_flux, TLOW)
plt.xlabel('Flux (W/m$^2$)')
plt.ylabel('Temperature (K)')
#plt.plot(FLUX, TGRY)
#plt.plot(FLUX, TAVG)
#plt.show()

plt.figure('Proportion of sublimation flux')
prop_sub_flux = np.array(sub_flux) / np.array(FLUX)
plt.subplot(211)
plt.plot(prop_sub_flux, TLOW)
plt.ylabel('temp (K)')
plt.subplot(212)
plt.plot(prop_sub_flux, FLUX)
plt.xlabel('sublimation flux / total flux ratio')
plt.ylabel('Flux (W/m$^2$)')


#print tilt[1]

for sh in range(len(shape_model)):
	for ti in range(len(tilt)):

		print shape_model[sh], tilt[ti]

		zen = tilt[ti] * np.pi/180.
		azi = off_axis_tilt[0] * np.pi/180.

		# CREATE SHAPE - XYZ for each facet centre and then area
		
		shape_points = oft.read_obj(shape_model[sh], 1)
		#ren, objekt = oft.vtk_object(shape_model[sh])
	
		Num = len(shape_points[0])
		print Num, len(shape_points[0]), len(shape_points[0])
		random_activity_factor = oft.activity_distributor(Num, random_flag, shape_points[0], x_lowlim, x_hilim, y_lowlim, y_hilim, z_lowlim, z_hilim) 
		#for r in range(Num):
		#	if r < Num/2. and r > Num/3.:
		#		random_activity_factor += 1.
		print random_activity_factor
	
		fig=plt.figure('Initial shape')
		ax = fig.add_subplot(111, projection='3d')
		ax.scatter(shape_points[6], shape_points[7], shape_points[8], c = random_activity_factor)
		#for i in range(len(shape_points[0])):
		#	ax.scatter(shape_points[0][i][0],shape_points[0][i][1],shape_points[0][i][2])
		ax.set_xlim(-2.5, 2.5)
		ax.set_ylim(-2.5, 2.5)
		ax.set_zlim(-2.5, 2.5)
		#plt.show()		
		
		# LOOP
		
		RH=[]
		RHexp=[]
		ORBIT=[]
		
		SUNX=[]
		SUNY=[]
		SUNZ=[]
	
		fig=plt.figure('Sun orbit')
		ax = fig.add_subplot(111, projection='3d')
		ax.set_xlim(-6, 6)
		ax.set_ylim(-6, 6)
		ax.set_zlim(-6, 6)

		orb=0.
		orbit_count = 0
		
		I_orbit_matrix = np.zeros((rh_steps,day_steps,Num))
		A_orbit_matrix = np.zeros((rh_steps,day_steps,Num))
		F_orbit_matrix = np.zeros((rh_steps,day_steps,Num))
		T_orbit_matrix = np.zeros((rh_steps,day_steps,Num))
		Z_orbit_matrix = np.zeros((rh_steps,day_steps,Num))
		Q_orbit_matrix = np.zeros((rh_steps,day_steps,Num))
		
		Time = []
		
		while orb<((2*np.pi)+orbint):
			Time.append(6.45 * (orb/(2*np.pi)) * 31536000.)
		        #Sun=np.array([((semimajoraxis-peri)+semimajoraxis*np.cos(orb))*np.cos(tilt),semiminoraxis*np.sin(orb),((semimajoraxis-peri)+semimajoraxis*np.cos(orb))*np.sin(tilt)])
			sun_x = ((semimajoraxis-peri)+semimajoraxis*np.cos(orb)) #*np.cos(tilt)
			sun_y = semiminoraxis*np.sin(orb) #*np.cos(off_axis_tilt)
			sun_z = 0. #((semimajoraxis-peri)+semimajoraxis*np.cos(orb)) #*np.sin(tilt) #+semiminoraxis*np.sin(orb)*np.sin(off_axis_tilt)
			Sun_prerot = [sun_x, sun_y, sun_z]
			axis_x = [1, 0, 0]
			axis_y = [0 ,1, 0]
			axis_z = [0, 0, 1]
			#Sun_rot1 = np.dot(oft.rotation_matrix(axis_y, zen), Sun_prerot)
			#Sun = np.dot(oft.rotation_matrix(axis_z, azi), Sun_rot1)
			Sun_rot1 = np.dot(oft.rotation_matrix(axis_z, azi), Sun_prerot)
			Sun = np.dot(oft.rotation_matrix(axis_y, zen), Sun_rot1)
			SUNX.append(Sun[0])
			SUNY.append(Sun[1])
			SUNZ.append(Sun[2])
			
		
			rh=np.sqrt(Sun[0]**2+Sun[1]**2+Sun[2]**2)
			print rh
			RH.append(rh)
		        RHexp.append(np.log10(rh))
			SUN_dayX=[]
			SUN_dayY=[]
			SUN_dayZ=[]
			orb_day=0.
			#orbint_day = np.pi/25
			day_count = 0
			while orb_day < ((2*np.pi)+orbint_day):
				rh_mag=np.sqrt(Sun[0]**2+Sun[1]**2)
				sun_day = np.array([rh_mag*np.cos(orb_day),  rh_mag*np.sin(orb_day), Sun[2]])
				SUN_dayX.append(sun_day[0])
				SUN_dayY.append(sun_day[1])
				SUN_dayZ.append(sun_day[2])
				#print sun_day
		
				#oft.vtk_camera_sun(sun_day[0], sun_day[1], sun_day[2], ren)
			
				for i in range(len(shape_points[1])):
					
					#shape_point = np.array((shape_points[0][i],shape_points[1][i],shape_points[2][i]))
		
					#for fc in range(len(shape_points[5])):
					#	intersect = isect_line_plane_v3(shape_point, sun_day, shape_points[5][fc], shape_points[6][fc], epsilon=1e-6)
					#	hull = ConvexHull((intersect, shape_points[8][shape_points[7][fc][0]], shape_points[8][shape_points[7][fc][1]], shape_points[8][shape_points[7][fc][2]]))
					#	if len(hull.simplices)==3:
					#		print len(hull.simplices)
					facet_area = shape_points[2][i] * 1000000.
					pr = oft.point2sun_angle(shape_points[1][i], shape_points[0][i], sun_day * 1.5e8)
		                	random_activity = random_activity_factor[i]
					if random_activity < random_activity_threshhold:
						Ill_ang = 90.
						FTZ = oft.FTZ_calculator(so, 1., 90., flux_percent, TLOW, ZLOW)
					else:
						Ill_ang = pr[0]
						FTZ = oft.FTZ_calculator(so, rh, pr[0], flux_percent, TLOW, ZLOW)
						#if Ill_ang < 90.:
						#	shadowing = 0
						#	for fc in range(len(shape_points[1])):
						#		if fc != i:	
						#			facet_intersect = oft.intersection(shape_points[0][i], Sun, shape_points[9][fc][0], shape_points[9][fc][1], shape_points[9][fc][2], shape_points[10][i])
						#			shadowing += facet_intersect
						#	print i, shadowing
						#	if shadowing == 0:
						#		FTZ = oft.FTZ_calculator(so, rh, pr[0], flux_percent, TLOW, ZLOW)
						#	else:
						#		Ill_ang = 90.
						#		FTZ = oft.FTZ_calculator(so, 1., 90., flux_percent, TLOW, ZLOW)
		        		I_orbit_matrix[orbit_count][day_count][i] += Ill_ang
					A_orbit_matrix[orbit_count][day_count][i] += np.cos(np.pi * Ill_ang / 180.) * facet_area
					F_orbit_matrix[orbit_count][day_count][i] += FTZ[0]
		                        T_orbit_matrix[orbit_count][day_count][i] += FTZ[1]
		                        Z_orbit_matrix[orbit_count][day_count][i] += FTZ[2]
					Q_orbit_matrix[orbit_count][day_count][i] += FTZ[2] * facet_area
		
				day_count += 1
				orb_day += orbint_day
		
			fig = plt.figure('Sun orbit')
			ax.plot(SUN_dayX,SUN_dayY,SUN_dayZ)
		
			orbit_count += 1
			orb+=orbint
		
		# PLOTTING RESULTS 
		
		print 'Plotting...'
		
		fig=plt.figure('Sun orbit')
		ax.plot(SUNX,SUNY,SUNZ)
		ax.scatter([0],[0],[0])
		#plt.show()
		
		#plt.figure('test')
		#for i in range(Num):
		#	plt.plot(T_orbit_matrix[0].T[i])
		
		#plt.figure('test 2')
		##for i in range(51):
		#	#for j in range(51):
		#	#	for k in range(Num):
		#for j in range(day_steps):
		#	#for k in range(Num):
		#	Tplot=[]
		#	for i in range(rh_steps):
		#		Tplot.append(Q_orbit_matrix[i][j][-1])
		#	plt.plot(RH,Tplot)
		#	plt.yscale('log')
			#print rsquared(np.log10(RH),np.log10(Tplot))[0]
		
		#plt.show()
		
		plot_these_distributions = [] #18, 21, 25, 36, 40, 44, 50]
		Q_rh=[]
		Q_rh_day=[]
		A_rh = []
		A_rh_day = []
		Z_rh = []
		Z_rh_day = []
		T_rh = []
		T_rh_day = []
		F_rh = []
		F_rh_day = []
		for i in range(rh_steps):
			Q_day = 0
			Q_day_hours=[]
			A_day = 0
			A_day_hours = []
			Z_day = 0
		        Z_day_hours=[]
			T_day = 0
		        T_day_hours=[]
			F_day = 0
		        F_day_hours=[]
			for j in range(day_steps):
				Q_hour=0
				A_hour = 0
				Z_hour = 0
				T_hour=0
				F_hour = 0
				for k in range(Num):
					#print i, j, k
					Q_hour += Q_orbit_matrix[i][j][k]
					A_hour += A_orbit_matrix[i][j][k]
					Z_hour += Z_orbit_matrix[i][j][k]
					T_hour += T_orbit_matrix[i][j][k]
					F_hour += F_orbit_matrix[i][j][k]
				#Z_hour /= Num
				Q_day_hours.append(Q_hour)
				A_day_hours.append(A_hour)
				Z_day_hours.append(Z_hour)
				T_day_hours.append(T_hour)
				F_day_hours.append(F_hour)
				Q_day += Q_hour
				A_day += A_hour
				Z_day += Z_hour
				T_day += T_hour
				F_day += F_hour
		
			for p in range(len(plot_these_distributions)):
				if i == plot_these_distributions[p]:
					plt.figure(str(i) + ' Flux distributions ' + str(Time[i]))
					plt.subplot(211)
					plt.title(str(RH[i]))
					plt.contourf(F_orbit_matrix[i])
					plt.ylabel('day interval')
					cbar = plt.colorbar()
					cbar.set_label('Flux (W/m$^2$)')
					plt.subplot(212)
					plt.contourf(T_orbit_matrix[i])
					plt.xlabel('facet no.')
		                        cbar = plt.colorbar()
					cbar.set_label('Temp (K)')
		
			Q_day /= day_steps
			A_day /= day_steps
			Z_day /= day_steps
			T_day /= day_steps
			F_day /= day_steps
			Q_rh.append(Q_day)
			Q_rh_day.append(Q_day_hours)
			A_rh.append(A_day)
			A_rh_day.append(A_day_hours)
			Z_rh.append(Z_day)
		        Z_rh_day.append(Z_day_hours)
			T_rh.append(T_day)
		        T_rh_day.append(T_day_hours)
			F_rh.append(F_day)
		        F_rh_day.append(F_day_hours)
		
		plt.figure('Total Q v Rh')
		plt.plot(RH,Q_rh)
		plt.xlabel('Heliocentric distance (AU)')
		plt.ylabel('Q (molec/s)')
		plt.yscale('log')
		#plt.xscale('log')
		plt.xlim(0,6)
		print Time
		print 'mass loss', (np.trapz(Time,Q_rh) * 0.018 / 6.e23), 'molecs/s * one comet year'
		
		plt.figure('Total A v Rh')
		plt.plot(RH,np.array(A_rh)/1000000)
		plt.xlabel('Heliocentric distance (AU)')
		plt.ylabel('Area (km$^2$)')
		#plt.yscale('log')
		plt.xlim(0,6)
	
		plt.figure('Total Z v Rh')
		plt.plot(RH,Z_rh)
		plt.xlabel('Heliocentric distance (AU)')
		plt.ylabel('Sublimation rate (molec/m$^2$/s)')
		plt.yscale('log')
		plt.xlim(0,6)
		#"""
		writer = open('./RES_81P/'+shape_model[sh][:4]+'_'+str(tilt[ti])+'.txt', 'w')
		for wr in range(len(RH)):
			writer.write(str(RH[wr]) + '   ' + str(A_rh[wr]) + '   ' + str(Z_rh[wr]) + '   ' + str(Q_rh[wr]) + '\n')
		writer.close()
		#"""
		#plt.figure('Total T v Rh')
		#plt.plot(RH,T_rh)
		#plt.figure('Total F v Rh')
		#plt.plot(RH,F_rh)
	
		#RH_splits = [24, 27, 20, 30, 15, 35, 10, 40, 5, 45] ##pi/25
		#RH_splits = [9, 12, 7, 14, 5, 16, 3, 18, 1, 20]
		#for i in range(5):
		#	print RH[RH_splits[2*i]], RH[RH_splits[1+i*2]], oft.rsquared(np.log10(RH[RH_splits[2*i]:RH_splits[1+i*2]]),np.log10(Q_rh[RH_splits[2*i]:RH_splits[1+i*2]]))[0], oft.rsquared(np.log10(RH[RH_splits[2*i]:RH_splits[1+i*2]]),np.log10(Z_rh[RH_splits[2*i]:RH_splits[1+i*2]]))[0]
		
		plt.figure('Z and Q slope')
		Bound_int = 3
		Bound_A = 0
		Bound_B = Bound_A + Bound_int
		while Bound_B < len(RH):
			Rh_part = RH[Bound_A:Bound_B]
			Z_part = Z_rh[Bound_A:Bound_B]
			Q_part = Q_rh[Bound_A:Bound_B]
			A_part = A_rh[Bound_A:Bound_B]
			Z_slope = oft.rsquared(np.log10(Rh_part), np.log10(Z_part))[0]
			Q_slope = oft.rsquared(np.log10(Rh_part), np.log10(Q_part))[0]
			A_slope = oft.rsquared(np.log10(Rh_part), np.log10(A_part))[0]
			if RH[Bound_B] > 5.6:
				plt.plot([RH[Bound_A],RH[Bound_B]],[Z_slope, Z_slope],color = 'red', label = 'Z')
				plt.plot([RH[Bound_A],RH[Bound_B]],[Q_slope, Q_slope],color = 'blue', label = 'Q')
				#plt.plot([RH[Bound_A],RH[Bound_B]],[A_slope, A_slope],color = 'green', label = 'A')
				plt.xlabel('Heliocentric distance (AU)')
				plt.ylabel('Slope')
			else:
				plt.plot([RH[Bound_A],RH[Bound_B]],[Z_slope, Z_slope],color = 'red')
		                plt.plot([RH[Bound_A],RH[Bound_B]],[Q_slope, Q_slope],color = 'blue')
				#plt.plot([RH[Bound_A],RH[Bound_B]],[A_slope, A_slope],color = 'green')
			print RH[Bound_A], RH[Bound_B], Z_slope, Q_slope, A_slope
			Bound_A += Bound_int
			Bound_B += Bound_int
	
		plt.legend()
		plt.xlabel('Heliocentric distance (AU)')
		plt.ylabel('Slope')
		#plt.show()
		


#i
