### IMPORTS

import datetime as dt
import matplotlib.pyplot as plt
import numpy as np
import orbit_function_toolbox as oft
import scipy.stats as sts

### CONSTANTS

Av=0.01
Avi=1.-Av
Ee=0.9
stbo=5.67e-8
Lice=2.6e6 #J/kg
L_co2 = 577000.

Rg=8.3144598 #J/Kmol
mol_water = 0.01801528 #kg/mol
mol_co2 = 0.04401

P0=3.56e12
E0=-6141.667

Flux=[]
Tempg=[]
Tempa=[]
Tempb=[]
Za=[]
Zb=[]
for i in range(1,200000,1):
	j=float(i)/10
	print j
	Tg=1000.
	cfg=oft.rad_flux(Tg)/Avi # sbl(Tg)/Avi
	while cfg>j:
		Tg-=0.1
		cfg = oft.rad_flux(Tg)/Avi
	Flux.append(j)
	Tempg.append(Tg)
	Ta=700.
	cfa=(oft.rad_flux(Ta) + oft.ZT_Lice(oft.general_vapour_pressure(Ta, 'CO2'), Ta, mol_co2, L_co2))/Avi
	while cfa>j:
		Ta-=0.1
		cfa = (oft.rad_flux(Ta) + oft.ZT_Lice(oft.general_vapour_pressure(Ta, 'CO2'), Ta, mol_co2, L_co2))/Avi
	Tempa.append(Ta)
	Tag=(Tg+Ta)/2.
	Tempb.append(Tag)
	Za.append(oft.ZT_Lice(oft.general_vapour_pressure(Ta, 'CO2'), Ta, mol_co2, 1.))
	Zb.append(oft.ZT_Lice(oft.general_vapour_pressure(Tag, 'CO2'), Tag, mol_co2, 1.))

plt.plot(Flux,Tempg,label='Greybody')
plt.plot(Flux,Tempa,label='With Z')
plt.plot(Flux,Tempb,label='Average')
plt.legend(loc=4)
plt.xlabel('Flux W/m$^2$')
plt.ylabel('Temperature K')
plt.show()

plt.plot(Flux,Za,label='With Z')
plt.plot(Flux,Zb,label='Average')
plt.xlabel('Flux W/m$^2$')
plt.ylabel('Vaporisation Rate molec/s/m$^2$')
plt.legend(loc=4)
plt.yscale('log')
plt.show()

fo=open('CalcFTZ_co2_higherF.txt','w')
for i in range(len(Flux)):
	fo.write(str(Flux[i]) + '   ' + str(Tempg[i]) + '   ' + str(Tempa[i]) + '   ' + str(Tempb[i]) + '   ' + str(Za[i]) + '   ' + str(Zb[i]) + '\n')
fo.close()

#
