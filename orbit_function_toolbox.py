import numpy as np
import scipy.stats as sts
import math
from scipy.spatial import ConvexHull
import vtk

emissivity = 0.9
stef_bol = 5.670e-8
R = 8.31446
mol_water = 0.018015 #kg
mol_co2 = 0.04401
Po = 3.56e12
T_top = 6141.667
Lice = 2.6e6
Lice_co2 = 577000. #J/kg
so=1367.

def rad_flux(T):
        return emissivity*stef_bol*T**4

def thermal_v(T, mol):
        top = 8.*R*T
        bottom = np.pi * mol
        return np.sqrt(top / bottom)

def water_vapor_pressure(T):
        return Po * np.exp(-1. * T_top / T) ##Pascal

def general_vapour_pressure(T, molecule):
	if molecule == 'H2O':
		A, B, C, D = -7.342973, -7.276391e3, 6.702455e1, 4.161914e-6
	elif molecule == 'CO2':
		A, B, C, D = -2.403761e1, -7.062404e3, 1.663861e2, 3.368548e-5
	return np.exp(A*np.log(T) + B/T + C + D*T**2)*1000.  ##Pascal 

def ZT_Lice(p, T, mol, L):
        PT = p
        vth = thermal_v(T, mol)
        return ( (2. * PT) / (np.pi * vth) ) * L

### DEFINITIONS 

def rsquared(x,y):
        slope, intercept, r_value, p_value, std_err = sts.linregress(x,y)
        return slope, std_err

def FTZ_calculator(solar_flux,helio_dist,point_angle, percent, TLOW, ZLOW):
        #Calculates flux reaching a point and gets corresponding temp and sublimation rate
        #Outputs: Flux, Temp, Sublimation rate
        flux=(solar_flux/helio_dist**2)*(np.cos(point_angle*np.pi/180.)) * percent
        fluxr=round(flux,1)
        arg=int((fluxr)*10)
        return flux, TLOW[arg], ZLOW[arg]/(0.018/6.022e23)

def sph2car(r,b,c):
        x=r*np.sin(b)*np.cos(c)
        y=r*np.sin(b)*np.sin(c)
        z=r*np.cos(b)
        return np.array([x, y, z])

def sphvec(Nd,r,Nr,rf, non_sph): #rotation fraction term
        downint=(np.pi)/Nd              #90/int
        roundint=(2*np.pi)/Nr
        R=[]
        Theta=[]
        Phi=[]
        point_height = []
        for i in np.arange(0,np.pi+downint,downint):
                rad = r - non_sph*np.sin(i)
                point_height.append(rad*np.cos(i))
                #print i, point_height
                if i == 0:
                        R.append(rad)
                        Theta.append(i)
                        Phi.append(0.)
                elif i == np.pi:
                        R.append(rad)
                        Theta.append(i)
                        Phi.append(0.)
                else:
                        for j in np.arange(0,2*np.pi,roundint):
                                R.append(rad)
                                Theta.append(i)
                                Phi.append(j+rf) #+rotation fraction
        band_heights=[]
        for i in range(len(point_height)-1):
                band_heights.append((point_height[i]+point_height[i+1])/2.)
        point_area=[]
        band_select=0
        for i in np.arange(0,np.pi+downint,downint):
                rad = r - non_sph*np.sin(i)
                if i == 0 or i == np.pi:
                        point_area.append(2*np.pi*rad*(point_height[0]-band_heights[0]))
                else:
                        for j in np.arange(0, 2*np.pi, roundint):
                                point_area.append(2*np.pi*rad*(band_heights[band_select]-band_heights[band_select+1])/Nr)
                        band_select += 1
                #print i, band_select

        Total_Area = 4*np.pi*r**2
        Total_band_area = np.sum(point_area)
        print Total_Area, Total_band_area*1000000

        sphx=[]
        sphy=[]
        sphz=[]
        for i in range(len(R)):
                #print sph2car(R[i], Theta[i], Phi[i])
                sphx.append(sph2car(R[i], Theta[i], Phi[i])[0])
                sphy.append(sph2car(R[i], Theta[i], Phi[i])[1])
                sphz.append(sph2car(R[i], Theta[i], Phi[i])[2])
        return sphx, sphy, sphz, point_area

def cosang(ax,ay,az,bx,by,bz,c): # A AND B(join) ARE COMPONENTS, C IS A VECTOR
        a=np.array([ax,ay,az])
        b=np.array([bx,by,bz])
        ba=a-b
        bc=c-b
        return np.dot(ba,bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))

def point2sun_angle(normpoint, point, sol):
        #calculates the angle between the vector to the point and sun sector
        #approximating small angles from large distances
        #Outputs: angle
	a = normpoint - point
	b = sol - point
        vector_cosangle = np.dot(a,b) / (np.linalg.norm(a) * np.linalg.norm(b))
        deg_vector_angle = angle(vector_cosangle)
        if deg_vector_angle < 90:
                output_angle = deg_vector_angle
        else:
                output_angle = 90
                #print vector_cosangle, deg_vector_angle, output_angle
        return output_angle, vector_cosangle

def angle(a):
        return np.arccos(a)*(180./np.pi)

# intersection function
def isect_line_plane_v3(p0, p1, p_co, p_no, epsilon=1e-6):
    """
    p0, p1: define the line
    p_co, p_no: define the plane:
        p_co is a point on the plane (plane coordinate).
        p_no is a normal vector defining the plane direction;
             (does not need to be normalized).

    return a Vector or None (when the intersection can't be found).
    """

    u = sub_v3v3(p1, p0)
    dot = dot_v3v3(p_no, u)
    #print u, dot

    if abs(dot) > epsilon:
        # the factor of the point between p0 -> p1 (0 - 1)
        # if 'fac' is between (0 - 1) the point intersects with the segment.
        # otherwise:
        #  < 0.0: behind p0.
        #  > 1.0: infront of p1.
        w = sub_v3v3(p0, p_co)
        fac = -dot_v3v3(p_no, w) / dot
        u = mul_v3_fl(u, fac)
        #print w, fac, u
	return add_v3v3(p0, u)
    else:
        # The segment is parallel to plane
        return None

# ----------------------
# generic math functions

def add_v3v3(v0, v1):
    return (
        v0[0] + v1[0],
        v0[1] + v1[1],
        v0[2] + v1[2],
        )


def sub_v3v3(v0, v1):
    return (
        v0[0] - v1[0],
        v0[1] - v1[1],
        v0[2] - v1[2],
        )


def dot_v3v3(v0, v1):
    return (
        (v0[0] * v1[0]) +
        (v0[1] * v1[1]) +
        (v0[2] * v1[2])
        )


def len_squared_v3(v0):
    return dot_v3v3(v0, v0)


def mul_v3_fl(v0, f):
    return (
        v0[0] * f,
        v0[1] * f,
        v0[2] * f,
        )

def cone_shape(height, radius, res):
	cone = vtk.vtkConeSource()
	cone.SetResolution(res)
	cone.SetDirection(0, 0, 1)
	cone.SetCenter(0, 0, 0)
	cone.SetHeight(height)
	cone.SetRadius(radius)
	cone.Update()
	polydata = cone.GetOutput()
	pointx = []
	pointy = []
	pointz = []
	for i in range(polydata.GetNumberOfPoints()):
		ids=vtk.vtkIdList()
		ids.SetNumberOfIds(polydata.GetCell(i).GetNumberOfPoints())
		
		point = polydata.GetPoint(i)
		pointx.append(point[0])
		pointy.append(point[1])
		pointz.append(point[2])
	
	facets = []
	for i in range(res):
		fac=[]
		fac.append(0)
		fac.append(i+1)
		if i+1==res:
			fac.append(1)
		else:
			fac.append(i+2)
		#print fac
		facets.append(fac)
	fac1= []
	for i in range(1,res):
		fac1.append(i)
	facets.append(fac1)
	facetx = []
	facety = []
	facetz = []
	for i in range(len(facets)):
		fx = 0.
		fy = 0.
		fz = 0.
		for j in range(len(facets[i])):
			fx += pointx[facets[i][j]]
			fy += pointy[facets[i][j]]
			fz += pointz[facets[i][j]]
		facetx.append(fx/len(facets[i]))
		facety.append(fy/len(facets[i]))
		facetz.append(fz/len(facets[i]))
	return facetx, facety, facetz

def read_obj(the_file, N):
	file_read = np.genfromtxt(the_file, dtype=None)
	points= []
	facets = []
	#print file_read[-1]
	for i in range(len(file_read)):
		if file_read[i][0] == 'v':
			pxyz = []
			for j in range(3):
				pxyz.append(file_read[i][j+1])
			points.append(pxyz)
		if file_read[i][0] == 'f':
			fxyz = []
			for j in range(3):
                                fxyz.append(int(file_read[i][j+1])-1)
			facets.append(fxyz)
			#print fxyz
	px = []
	py = []
	pz = []
	p = []
	areax = []
	normals = []
	centre_points = []
	centre_pointsx = []
	centre_pointsy = []
	centre_pointsz = []
	normal_points = []
	norm_a = []
	facet_points=[]
	normal_vectors = []
	#print points[0]
	for i in range(len(facets)/N):
		#print i, facets[i]
		a = np.array(points[facets[i*N][0]])
		b = np.array(points[facets[i*N][1]])
		c = np.array(points[facets[i*N][2]])
		fp = [a, b, c]
		facet_points.append(fp)
		centre_point = (a + b + c) / 3.
		#centre_point = [(a[0]+b[0]+c[0])/3., (a[1]+b[1]+c[1])/3., (a[2]+b[2]+c[2])/3.]
		normal = np.cross((b-centre_point),(c-centre_point))
		normal_vectors.append(normal)
		normal4area = np.cross((b-a),(c-a))
		area = np.linalg.norm(normal4area)/2.
		normal_point = centre_point + normal
		norm_a.append(normal4area)
		centre_points.append(centre_point)
		centre_pointsx.append(centre_point[0])
		centre_pointsy.append(centre_point[1])
		centre_pointsz.append(centre_point[2])
		normal_points.append(normal_point)
		#print i, len(facets), point, area
		#px.append(point[0])
		#py.append(point[1])
		#pz.append(point[2])
		p.append(a)
		areax.append(area)
	#print len(facets)
	Total_Area = np.sum(areax) #52 km2
	print 'Area: ', Total_Area, Total_Area * 1000000
	areax_weighted = areax / Total_Area
	return centre_points, normal_points, areax, facets, points, norm_a, centre_pointsx, centre_pointsy, centre_pointsz, facet_points, normal_vectors
	#return px, py, pz, areax, areax_weighted, normals, normal_points, facets, points, p

def activity_distributor(Num, random_flag, points, x_lowlim, x_hilim, y_lowlim, y_hilim, z_lowlim, z_hilim):
	if random_flag == 0:
		distribution = np.ones(Num)
	elif random_flag == 1:
		distribution = np.random.random(Num)
	else:
		distribution = np.zeros(Num)
		for r in range(Num):
			#print points[r]
			if points[r][0] > x_lowlim and points[r][0] < x_hilim and points[r][1] > y_lowlim and points[r][1] < y_hilim and points[r][2] > z_lowlim and points[r][2] < z_hilim:
				distribution[r] += 1.
			else:
				distribution[r] += 0.

	return distribution

def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    axis = axis/math.sqrt(np.dot(axis, axis))
    a = math.cos(theta/2.0)
    b, c, d = -axis*math.sin(theta/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])

def intersection(centre, sun, fp1, fp2, fp3, normal):
	intersect = isect_line_plane_v3(centre, sun, fp1, normal)
	hull = ConvexHull((centre, sun, intersect, fp1, fp2, fp3))
	if len(hull.vertices) == 5:
		return 0
	else:
		return 1

def vtk_object(objfile):
	objekt = vtk.vtkOBJReader() #vtk.vtkPolyDataReader()
	objekt.SetFileName(objfile)
	#objekt.ReadAllScalarsOn()
	objekt.Update()
	
	data = objekt.GetOutput()
	facets = data.GetNumberOfPolys()
	print facets

	fcolour = []
	celldata = vtk.vtkFloatArray()
	for i in range(facets):
		celldata.InsertNextValue(int(i))
		fcolour.append(float(i))
	lut = vtk.vtkLookupTable()
	lut.SetNumberOfTableValues(facets)
	lut.SetHueRange(0.0,0.667)
	lut.Build()

	for i in range(facets):
		fc = fcolour[i]/facets
		lut.SetTableValue(i,1,1,1,1)

	objekt.Update()
	#return objekt

#def vtk_camera_sun(a, b, c):
	objekt.GetOutput().GetCellData().SetScalars(celldata)

	transform = vtk.vtkTransform()
	transformFilter = vtk.vtkTransformPolyDataFilter()
	transformFilter.SetTransform(transform)
	transformFilter.SetInputConnection(objekt.GetOutputPort())
	transformFilter.Update()
	
	CG_mapper = vtk.vtkPolyDataMapper()
	CG_mapper.SetInputConnection(transformFilter.GetOutputPort())
	CG_mapper.SetScalarRange(0, facets)
	CG_mapper.SetLookupTable(lut)

	CG_Actor = vtk.vtkActor()
	CG_Actor.SetMapper(CG_mapper)

	ren = vtk.vtkRenderer()
	ren.SetBackground(0,0,0)
	ren.AddActor(CG_Actor)
	return ren, objekt
	
def vtk_camera_sun(a, b, c, ren):
	camera = vtk.vtkCamera()
	camera.SetPosition(2*a, 2*b, 2*c)
	camera.SetFocalPoint(1, 1, 1)
	ren.SetActiveCamera(camera)
	ren.SetUseShadowsOff()

	renWin = vtk.vtkRenderWindow()
	renWin.SetWindowName("67P/CG")
	renWin.SetSize(1600, 2400)
	renWin.AddRenderer(ren)
	#renWin.UseShadowsOff()

	#print ren #.GetProps()
	#print renWin.GetRGBAPixelData(0,0,1,1,objekt,0)
	#print renWin.GetZbufferDataAtPoint(400,400)
	#print ren.GetPixelData(0,0,10,10,0,0)

	iren = vtk.vtkRenderWindowInteractor()
	iren.SetRenderWindow(renWin)
	
	#axes = vtk.vtkAxesActor()
	#ax = vtk.vtkOrientationMarkerWidget()
	#ax.SetOrientationMarker(axes)
	#ax.SetInteractor(iren)
	#ax.EnabledOn()
	
	iren.Initialize()
	iren.Start()
	

def comet_file_open(the_file):
        comet_file = np.genfromtxt(the_file, dtype = None)
        return comet_file

#i
